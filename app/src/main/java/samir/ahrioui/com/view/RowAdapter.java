package samir.ahrioui.com.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import samir.ahrioui.com.R;
import samir.ahrioui.com.model.CountryList;

public class RowAdapter extends ArrayAdapter<String> {

    public RowAdapter(Context context, int resource, String[] countries) {
        super(context, resource, countries);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.activity_rowview, parent, false);
        }

        RowViewHolder viewHolder = (RowViewHolder) convertView.getTag();
        if (viewHolder == null) {
            viewHolder = new RowViewHolder();
            viewHolder.mCountryName = (TextView) convertView.findViewById(R.id.country_name_row);
            viewHolder.mCountryFlag = (ImageView) convertView.findViewById(R.id.country_flag_row);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la String[] countries
        String countryName = getItem(position);
        String countryFlag = CountryList.getCountry(countryName).getmImgFile();

        //il ne reste plus qu'à remplir notre vue

        viewHolder.mCountryName.setText(countryName);
        viewHolder.mCountryFlag.setImageDrawable(CountryList.getImage(RowAdapter.this.getContext(),countryFlag));

        return convertView;
    }

    private class RowViewHolder {
        public TextView mCountryName;
        public ImageView mCountryFlag;
    }

}
