package samir.ahrioui.com.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import samir.ahrioui.com.model.CountryList;
import samir.ahrioui.com.R;
import samir.ahrioui.com.view.RowAdapter;

public class MainActivity extends AppCompatActivity {
    private ListView mListView;
    private String[] mCountries = CountryList.getNameArray();
    public static final String SELECTED_COUNTRY = "SELECTED_COUNTRY";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.out.println("MainActivity :: onCreate()");
        mListView = findViewById(R.id.main_activity_list);

        final RowAdapter adapter = new RowAdapter
                (MainActivity.this,R.layout.support_simple_spinner_dropdown_item,mCountries);
        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String clickedCountry = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this,CountryDataActivity.class);
                intent.putExtra(SELECTED_COUNTRY, clickedCountry);
                startActivity(intent);

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("MainActivity :: onStart()");

    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("MainActivity :: onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("MainActivity :: onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("MainActivity :: onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("MainActivity :: onDestroy()");
    }
}
