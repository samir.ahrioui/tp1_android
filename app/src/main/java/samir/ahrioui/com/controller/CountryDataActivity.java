package samir.ahrioui.com.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import samir.ahrioui.com.R;
import samir.ahrioui.com.model.Country;
import samir.ahrioui.com.model.CountryList;

public class CountryDataActivity extends AppCompatActivity {
    private Intent mIntent;
    private String mNameSelectedCountry;
    private ImageView mFlag;
    private TextView mCountryName;
    private TextView mCapitale;
    private TextView mLangues;
    private TextView mMonnaie;
    private TextView mPopulation;
    private TextView mSuperficie;
    private Country mCountry;
    private Button mButtonSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_data);
        //get value of selected country in Main activity
        mIntent= getIntent();
        mNameSelectedCountry = mIntent.getStringExtra(MainActivity.SELECTED_COUNTRY);
        mCountry = CountryList.getCountry(mNameSelectedCountry);
        // find fields activity by id
        mCountryName = findViewById(R.id.ac2_country_name);
        mFlag = findViewById(R.id.ac2_image_flag);
        mCapitale = findViewById(R.id.ac2_capitale);
        mLangues = findViewById(R.id.ac2_langues);
        mMonnaie = findViewById(R.id.ac2_monnaie);
        mPopulation = findViewById(R.id.ac2_population);
        mSuperficie = findViewById(R.id.ac2_superficie);
        mButtonSave = findViewById(R.id.ac2_btn_sauvegarder);

        // fill fields activity
        mCountryName.setText(mNameSelectedCountry);
        mFlag.setImageDrawable(CountryList.getImage(this, mCountry.getmImgFile()));
        mCapitale.setText(mCountry.getmCapital());
        mLangues.setText(mCountry.getmLanguage());
        mSuperficie.setText( Integer.toString(mCountry.getmArea()));
        mMonnaie.setText(mCountry.getmCurrency());
        mPopulation.setText(Integer.toString(mCountry.getmPopulation()));

        // Button save Action

        mButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mCountry.setmArea(Integer.parseInt(mSuperficie.getText().toString()));
                mCountry.setmPopulation(Integer.parseInt(mPopulation.getText().toString()));
                mCountry.setmCapital(mCapitale.getText().toString());
                mCountry.setmLanguage(mLangues.getText().toString());
                mCountry.setmCurrency(mMonnaie.getText().toString());

                CountryList.saveCountry(mNameSelectedCountry,mCountry);
                Toast.makeText(CountryDataActivity.this, "Sauvegardé!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
